1. buat database
CREATE DATABASE MYSHOP;

2. buat tabel
CREATE TABLE users(
    id int AUTO_INCREMENT PRIMARY KEY,
    name varchar(255),
    email varchar(255),
    password varchar(255)
);

CREATE TABLE categories(
    id int AUTO_INCREMENT PRIMARY KEY,
    name varchar(255)
);

CREATE TABLE items( 
    id int AUTO_INCREMENT PRIMARY KEY, 
    name varchar(255), 
    description varchar(255), 
    price int, stok int, 
    category_id int, 
    FOREIGN KEY (category_id) REFERENCES categories(id) 
);

3. memasukkan data
INSERT INTO users (name, email, password) 
VALUES ('John Doe', 'john@doe.com', 'john123'), 
('Jane Doe', 'jane@doe.com', 'jenita123');

INSERT INTO categories(name) 
VALUES ('gadget'), ('cloth'), ('men'), ('woman'), ('branded')

INSERT INTO items(name, description, price, stok, category_id) 
VALUES ('sumsang b50', 'hape keren', 4000000, 100, 1), 
('uniklooh', 'baju keren', 5000000, 50, 2), 
('IMHO watch', 'jam tangan anak keren', 2000000, 10, 1);

4. mengambil data dari database
SELECT id, name, email FROM users

SELECT * FROM items 
WHERE price>1000000
SELECT * FROM items WHERE name LIKE '%watch'

SELECT items.name, items.description, items.price, items.stok, items.category_id, categories.name 
FROM items 
LEFT JOIN categories ON items.category_id = categories.id 
ORDER BY categories.name

5. mengubah data
UPDATE items SET price = 2500000 WHERE id = 1