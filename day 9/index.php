<?php

// Bentuk fungsi
require_once ('animal.php');
require_once ('frog.php');
require_once ('ape.php');


$sheep = new animal("shaun");

echo "Nama Hewan : " . $sheep->name . "<br>"; // "shaun"
echo "Jumlah Kaki : " .$sheep->legs. "<br>"; // 4
echo "Berdarah Dingin : " .$sheep->cold_blooded. "<br><br>"; // "no"

$sungokong = new ape("kera sakti");
echo "Nama Hewan : " . $sungokong->name . "<br>"; // "shaun"
echo "Jumlah Kaki : " .$sungokong->legs. "<br>"; // 4
echo "Berdarah Dingin : " .$sungokong->cold_blooded. "<br>"; // "no"
$sungokong->yell("Auooo"); // "Auooo"


$kodok = new frog("buduk");
echo "Nama Hewan : " . $kodok->name . "<br>"; // "shaun"
echo "Jumlah Kaki : " .$kodok->legs. "<br>"; // 4
echo "Berdarah Dingin : " .$kodok->cold_blooded. "<br>"; // "no"
$kodok->jump() ; // "hop hop"
?>