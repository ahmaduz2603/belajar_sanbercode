@extends('layout.master')
@section('judul')
Halaman Update Cast
@endsection
@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="name" class="form-control" value="{{old('name',$cast->name)}}">
    </div>
    @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>umur</label>
      <input type="number" name="umur" class="form-control" value="{{old('umur',$cast->umur)}}">
    </div>
    <div class="form-group">
        <label>Bio</label>
        <textarea name="bio" cols="30" rows="10" class="form-control"> {{old('bio',$cast->bio)}}</textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection