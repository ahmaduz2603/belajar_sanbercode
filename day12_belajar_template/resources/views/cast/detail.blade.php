@extends('layout.master')
@section('judul')
Halaman Detail Cast
@endsection
@section('content')

<h1 class="text-primary">{{$cast->name}}</h1>
<h1>{{$cast->umur}}</h1>
<p>{{$cast->bio}}</p>

<a href="/cast" class="btn btn-primary btn-sm mt-4">Kembali</a>

@endsection