<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/table', 'TableController@table')->name('table');
Route::get('/datatable', 'DatatableController@datatable')->name('datatable');
Route::get('/master', function(){
    return view('layout.master');
});
//CRUD Casr
//Create Data Cast


//Masuk ke form cast 
Route::get('/cast/create', 'CastController@create');
//kirim data
Route::post('/cast', 'CastController@store');

//Tampil ke form cast 
Route::get('/cast', 'CastController@index');
Route::get('/cast/{id}', 'CastController@show');
Route::get('/cast/{id}/edit', 'CastController@edit');
Route::put('/cast/{id}', 'CastController@update');
Route::delete('/cast/{id}', 'CastController@delete');