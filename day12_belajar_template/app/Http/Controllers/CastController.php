<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create(){
        return view('cast.create');
    }

    public function store(Request $request){
        // dd($request->all());
        $request->validate([
            'name' => 'required',
            'umur' => 'required|max:2',
            'bio' => 'required',
        ],
        [
            'name.required' => 'Nama Kosong',
        ]
        );
        DB::table('cast')->insert(
            [
            'name' => $request['name'], 
            'umur' => $request['umur'],
            'bio' => $request['bio']
            ]
        );  
        return redirect('/cast');
    }
    
    public function index(){
        $cast = DB::table('cast')->get();
 
        return view('cast.tampil', ['cast' => $cast]);
    }

    public function show($id){
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('cast.detail', compact('cast'));
    }

    public function edit($id){
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('cast.edit', compact('cast'));
    }

    public function update(Request $request, $id){
        $request->validate([
            'name' => 'required',
            'umur' => 'required|max:2',
            'bio' => 'required',
        ],
        [
            'name.required' => 'Nama Kosong',
        ]
        );
        DB::table('cast')
              ->where('id', $id)
              ->update(
                  [
                      'name' => $request['name'],
                      'umur' => $request['umur'],
                      'bio' => $request['bio']
                  ]
            );

        return redirect("/cast");
    }
    public function delete($id){
        DB::table('cast')->where('id', $id)->delete();

        return redirect("/cast");
    }
}
